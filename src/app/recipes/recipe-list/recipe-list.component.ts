import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import {Recipe} from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  @Output() recipeWasSelected = new EventEmitter<Recipe>();
  recipes: Recipe[] = [
    new Recipe('A Test Recipe', 'This is simply a test', 'https://wgbh.brightspotcdn.com/dims4/default/09cb564/2147483647/strip/true/crop/1359x741+0+0/resize/990x540!/quality/70/?url=https%3A%2F%2Fwgbh.brightspotcdn.com%2Fcb%2F02%2F098fb19f47b9a56d92bc18b8a279%2Fcooking-apps-lead.jpg'),
    new Recipe('Another Test Recipe', 'This is simply a test', 'https://wgbh.brightspotcdn.com/dims4/default/09cb564/2147483647/strip/true/crop/1359x741+0+0/resize/990x540!/quality/70/?url=https%3A%2F%2Fwgbh.brightspotcdn.com%2Fcb%2F02%2F098fb19f47b9a56d92bc18b8a279%2Fcooking-apps-lead.jpg')
  ];

  constructor() { }

  ngOnInit(): void {
  }

  onRecipeSelected(recipe: Recipe): void {
    this.recipeWasSelected.emit(recipe);
  }
}
